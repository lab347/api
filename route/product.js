const productRoute = require('express').Router();
productRoute.get('/', (req, res) => {
    res.json({
        success: true
    })
})
productRoute.post('/newproduct', (req, res) => {
    const { product_name, product_size, product_colors } = req.body
    res.json({
        success: true,
        product_name,
        product_size,
        product_colors
    })
})
module.exports = productRoute